package values;

public class BoolValue implements IValue {
	private boolean value;
	
	public BoolValue(boolean value) {
		this.value = value;
	}

	public boolean getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Boolean.toString(value);
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof BoolValue){
			return
					this.value == ((BoolValue) obj).getValue();
		}
		else
			return false;
	}
	
}
