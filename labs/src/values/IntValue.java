package values;

public class IntValue implements IValue {
	private int value;
	
	public IntValue(int value) {
		this.value = value;
	}

	public IntValue(boolean b) {
		// TODO Auto-generated constructor stub
	}

	public int getValue() {
		return value;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
	
	@Override

		public boolean equals(Object obj) {
			return obj instanceof IntValue && value == ((IntValue)obj).getValue();
		}
	
}
