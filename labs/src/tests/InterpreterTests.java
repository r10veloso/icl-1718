package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class InterpreterTests {

	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	 private void testCaseBool(String expression, boolean value) throws ParseException {
		 //TODO this is incomplete  (window-> show view-> Tasks)
	 	assertTrue(Console.acceptCompareBool(expression, new BoolValue(value)));		
		 }
		
	
	private void testNegativeCase(String expression, IntValue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression, value));
	}
	
	// private void testCaseBool(String expression, boolean value) throws ParseException {
	// 	assertTrue(Console.acceptCompareBool(expression, new BoolValue(value)));		
	// }
	
	// private void testNegativeCaseBool(String expression, boolean value) throws ParseException {
	// 	assertFalse(Console.acceptCompareBool(expression, new BoolValue(value)));
	// }
	
	@Test
	public void test01() throws Exception {
		testCase("1\n",new IntValue(1));
		testCase("1+2\n",new IntValue(3));
		testCase("1-2-3\n",new IntValue(-4));
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		testCase("4*2\n",new IntValue(8));
		testCase("4/2/2\n",new IntValue(1));
		testCase("-1\n", new IntValue(-1));
		testCase("-1*3\n",new IntValue(-3));
		testCaseBool("true\n",true);
		testCaseBool("false\n",false);
		 testCaseBool("11 < 22\n", true);
		 testCaseBool("11 > 22\n", false);
		// testCaseBool("11 == 22\n", false);
		// testCaseBool("3*5 != 1+2 == true\n", true);
		// testCaseBool("1 == 2 && 3 == 4\n", false);
		// testCaseNegativeBool("1 == 2 || 3 == 4 && xpto \n", true);
		// testCaseNegativeBool("!(1 == 2) && xpto \n", true);
	}
	
	@Test
	public void testsLabClass05() throws Exception {
<<<<<<< HEAD
		/*
=======
>>>>>>> a96559373ccc6bbf681ae833bed9983c3485fcbe
		testCase("decl x = 1 in x+1 end\n",2);
		testCase("decl x = 1 in decl y = 2 in x+y end end\n",3);
		testCase("decl x = decl y = 2 in 2*y end in x*3\n",4);
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end\n", 5);
		testNegativeCase("x+1", 0);
		testNegativeCase("decl x = 1 in x+1 end + x", 0);
<<<<<<< HEAD
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end\n", 5);	
		*/
=======
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end\n", 5);		
>>>>>>> a96559373ccc6bbf681ae833bed9983c3485fcbe
	}
}
