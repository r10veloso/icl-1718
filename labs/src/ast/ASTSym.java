package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import values.IValue;
import values.IntValue;

public class ASTSym implements ASTNode {

	//int val;
	 ASTNode left; 	//= new ASTSym(0);
	

	public ASTSym(ASTNode l){			//(int n) {
		left = l;
		//val = n;  //left = n;
	}
//
	@Override
	public String toString() {
		return "- " +left.toString();
	}

	
	@Override
	public IValue eval() {
		IValue l = left.eval();
		
		if( l instanceof IntValue){			
			return new IntValue( - ((IntValue)l).getValue());
		}else
			return null;
		}
	@Override
	public IType typecheck() throws TypingException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}
	
}
