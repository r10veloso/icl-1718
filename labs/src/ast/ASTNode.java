package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import values.IValue;
import values.IntValue;

public interface ASTNode {
	
	IValue eval();

	//Seeccionar "int" acima -> Refactor -> change signature vale -> IValue

	
	IType typecheck() throws TypingException;
	

	void compile(CodeBlock code);


//	void compile(CodeBlock code);

}

