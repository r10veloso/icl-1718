package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import values.IValue;
import values.IntValue;

public class ASTAdd implements ASTNode {

	ASTNode left, right;

	public ASTAdd(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public String toString() {
		return left.toString() + " + " + right.toString();
	}

	
 	@Override
	public IValue eval() {
		IValue l = left.eval();
		IValue r = right.eval();

		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue()+((IntValue)r).getValue());
		else 
			return null; // TODO WRONG AND INCOMPLETE

//		else 

//			throw new TypeMismatchException("Wrong types in add");
 	}

	@Override
	public IType typecheck() throws TypingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void compile(CodeBlock code) {
		// TODO Auto-generated method stub
		
	}

/*
 	@Override
	public IType typecheck() throws TypingException {
 		IType l = left.typecheck();
 		IType r = right.typecheck();

		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue()+((IntValue)r).getValue());
		else 
			return null; // TODO WRONG AND INCOMPLETE

//		else 

//			throw new TypeMismatchException("Wrong types in add");
 	}

*/

}
